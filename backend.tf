terraform {
  backend "gcs" {
    bucket  = "tfstate-bucket1"
    prefix  = "terraform/state"
  }
}
