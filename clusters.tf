resource "google_container_cluster" "primary" {
  name     = "${local.settings.project}-${terraform.workspace}-cluster"
  location = local.settings.region
  provider = google
  remove_default_node_pool = true
  initial_node_count       = 1
  network = google_compute_network.vpc.name
  subnetwork = google_compute_subnetwork.subnet.name
  logging_service = "none"
  monitoring_service = "none"
  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }
}

resource "google_container_node_pool" "infra_nodes" {
  name       = "infra-pool"
  location   = local.settings.region
  provider   = google
  cluster    = google_container_cluster.primary.name
  node_count = local.settings.pools.infra.number_of_nodes
  project    = local.settings.project_id
  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    labels = {
      env = terraform.workspace
    }

    machine_type = local.settings.pools.infra.machine_type
    tags         = ["gke-node", "${local.settings.project}"]
    metadata = {
      disable-legacy-endpoints = "true"
    }

    taint {
      key = "node-role"
      value = "infra"
      effect = "NO_SCHEDULE"
    }

  }
  node_locations = ["${local.settings.region}-b", "${local.settings.region}-c"]
}

resource "google_container_node_pool" "app_nodes" {
  name       = "app-pool"
  location   = local.settings.region
  provider   = google
  cluster    = google_container_cluster.primary.name
  node_count = local.settings.pools.app.number_of_nodes
  project    = local.settings.project_id
  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    labels = {
      env = terraform.workspace
    }

    machine_type = local.settings.pools.app.machine_type
    tags         = ["gke-node", "${local.settings.project}"]
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }
  node_locations = ["${local.settings.region}-b", "${local.settings.region}-c"]
}

output "kubernetes_cluster_name" {
  value       = google_container_cluster.primary.name
  description = "GKE Cluster Name"
}

