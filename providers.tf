provider "google" {
  project = local.settings.project_id
  region  = local.settings.region
#  credentials = var.creds
}

provider "google-beta" {
  project = local.settings.project_id
  region  = local.settings.region
#  credentials = var.creds
}
