# VPC
resource "google_compute_network" "vpc" {
  name                    = "${local.settings.project}-${terraform.workspace}-vpc"
  auto_create_subnetworks = "false"
  project                 = local.settings.project_id
}

# Subnet
resource "google_compute_subnetwork" "subnet" {
  name          = "${local.settings.project}-${terraform.workspace}-subnet"
  region        = local.settings.region
  network       = google_compute_network.vpc.name
  ip_cidr_range = local.settings.vpc_cidr
  project       = local.settings.project_id
}
