resource "google_dns_managed_zone" "dns_zone" {
  name        = replace(local.settings.domain, ".", "-")
  dns_name    = "${local.settings.domain}."
  description = "DNS zone for ${terraform.workspace} environment"
  labels = {
    env = terraform.workspace
  }
}

output "dns_zone_nameservers" {
  value       = google_dns_managed_zone.dns_zone.name_servers
  description = "DNS nameservers"
}
