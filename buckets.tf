resource "google_storage_bucket" "state_bucket" {
  name          = var.state_bucket
  location      = "EU"
  force_destroy = true
  versioning {
    enabled = true
  }
}
